<?php 
// $hello = 'Hello world!';
class Building {
	//if the access modifier of the property is private, you cannot directly access its value

	//if the access modifier is private the child class won't inherit the properties
	private $name;
	private $floors;
	private $address;

	public function __construct($name, $floors, $address){
		$this->name = $name;
		$this->floors = $floors;
		$this->address = $address;
	}

	public function getName(){
		return $this->name;
	}

	public function setName($name){
		$this->name = $name;
	}

	public function getFloors(){
		return $this->floors;
	}
	public function getAddress(){
		return $this->address;
	}
}

class Condominium extends Building{

}


$building = new Building('Basic Building', 21, 'Manila City, Manila');

$condominium = new Condominium('Basic Condominium', 100, 'Quezon City, Manila');


 ?>